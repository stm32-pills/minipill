MiniPill
========

Mithat Konar

MiniPill is an easy to assemble, "Blue Pill"-compatible STM32F103C8T6 development board with minimal features. To make the board _almost_ size-compatible with the original Blue Pill, USB functionality and the power LED have been removed.

![MiniPill top](./images/MiniPill-top.png)  ![MiniPill top](./images/MiniPill-bottom.png)

The board uses 0805 surface mount parts and a few through hole parts that can be easily obtained as generic components. All SMD assembly is on the top side of the board, making it possible to assemble the board at minimal cost.

MiniPill is intended as an alternative if your project doesn't need USB and/or if good Blue Pills become unavailable.

This work is derived from [STM32surface](https://github.com/profdc9/STM32surface) copyrighted by Daniel Marks (KW4TI) and licensed under the Creative Commons License CC-BY-SA 4.0.
